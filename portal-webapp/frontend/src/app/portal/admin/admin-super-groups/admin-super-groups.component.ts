import { Component, OnInit } from '@angular/core';
import { PageSuperGroupService } from '../../../generated/service/page-super-group.service';
import { PageSuperGroup } from '../../../shared/model/page-super-group.model';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-super-groups',
  templateUrl: './admin-super-groups.component.html',
  styleUrls: ['./admin-super-groups.component.css']
})
export class AdminSuperGroupsComponent implements OnInit {
  pageSuperGroups: PageSuperGroup[];
  loading: boolean;

  constructor(
    private pageSuperGroupService: PageSuperGroupService,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    // todo: find all page super groups and show on page
    this.loading = true;
    this.pageSuperGroupService
      .findAll()
      .subscribe((pageSGs: PageSuperGroup[]) => {
        this.pageSuperGroups = pageSGs;
        this.loading = false;
      });
  }

  onEditPageGroups(pageSG: PageSuperGroup) {
    this.router.navigate([pageSG.id, 'page-groups'], {
      relativeTo: this.route
    });
  }

  onSavePageSuperGroup(pageSG: PageSuperGroup) {
    this.pageSuperGroupService
      .savePageSuperGroup(pageSG)
      .subscribe((pageSGUpdated: PageSuperGroup) => {
        this.snackBar.open(pageSGUpdated.name + ' was saved!');
        for (let i = 0; i < this.pageSuperGroups.length; i++) {
          if (this.pageSuperGroups[i].id === pageSGUpdated.id) {
            this.pageSuperGroups[i] = pageSGUpdated;
          }
        }
      });
  }

  onNewPageSuperGroup() {
    const newPageSG = new PageSuperGroup();
    newPageSG.name = 'New Page Super Group';
    newPageSG.description = 'Do not forget to save';
    this.pageSuperGroups.push(newPageSG);
  }
}
