import { Component, OnInit } from '@angular/core';
import { PageSuperGroup } from '../../../shared/model/page-super-group.model';
import { PageGroup } from '../../../shared/model/page-group.model';
import { Router, ActivatedRoute } from '@angular/router';
import { PageSuperGroupService } from '../../../generated/service/page-super-group.service';
import { MatSnackBar } from '@angular/material';
import { PageGroupService } from '../../../generated/service/page-group.service';
import { Page } from '../../../shared/model/page.model';

@Component({
  selector: 'app-admin-pages',
  templateUrl: './admin-pages.component.html',
  styleUrls: ['./admin-pages.component.css']
})
export class AdminPagesComponent implements OnInit {
  pageSuperGroup: PageSuperGroup;
  pageGroup: PageGroup;
  loading: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pageSGService: PageSuperGroupService,
    private pageGroupService: PageGroupService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    // set the super group & page group
    this.loading = true;
    this.pageSGService
      .getPageSuperGroup(this.route.snapshot.params['pageSuperGroupId'])
      .subscribe((pageSG: PageSuperGroup) => {
        this.pageSuperGroup = pageSG;
        this.pageGroup = pageSG.getPageGroup(this.route.snapshot.params['pageGroupId']);
        this.loading = false;
      });
  }

  onSavePage(page: Page) {
    this.pageSGService
      .savePageSuperGroup(this.pageSuperGroup)
      .subscribe((pageSGUpdated: PageSuperGroup) => {
        this.snackBar.open(page.name + ' was saved!');
        this.pageSuperGroup = pageSGUpdated;
        this.pageGroup = pageSGUpdated.getPageGroup(this.route.snapshot.params['pageGroupId']);
      });
  }

  onNewPage() {
    const newPage = new Page();
    newPage.name = 'New Page Group';
    newPage.description = 'Do not forget to save';
    this.pageGroup.pages.push(newPage);
  }

  onMatIconInfo() {
    window.open('https://material.io/tools/icons', '_blank');
  }
}
