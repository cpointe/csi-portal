import { Component, OnInit } from '@angular/core';
import { PortalUserService } from '../../../generated/service/portal-user.service';
import { PortalUser } from '../../../shared/model/portal-user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Portal } from '@angular/cdk/portal';
import { MatSnackBar, MatCheckboxChange } from '@angular/material';
import { PageSuperGroupService } from '../../../generated/service/page-super-group.service';
import { PageSuperGroup } from '../../../shared/model/page-super-group.model';
import { PageGroup } from '../../../shared/model/page-group.model';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {
  loadingUser = true;
  loadingPageSGs = true;
  user: PortalUser;
  pageSGs: PageSuperGroup[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private userService: PortalUserService,
    private pageSuperGroupService: PageSuperGroupService
  ) {}

  ngOnInit() {
    const userId = this.route.snapshot.params['userId'];
    if (userId === 'new') {
      this.user = new PortalUser();
      this.user.displayName = 'Add Display Name';
      this.user.admin = false;
      this.user.active = true;
      this.loadingUser = false;
    } else {
      this.userService.getPortalUser(userId).subscribe((user: PortalUser) => {
        this.user = user;
        this.loadingUser = false;
      });
    }

    this.pageSuperGroupService
      .findAll()
      .subscribe((pageSGs: PageSuperGroup[]) => {
        this.pageSGs = pageSGs;
        this.loadingPageSGs = false;
      });
  }

  onSaveUser() {
    this.userService.savePortalUser(this.user).subscribe((user: PortalUser) => {
      this.user = user;
      this.snackBar.open(user.displayName + ' was updated!');
    });
  }

  onEditPageGroup(pageSG: PageSuperGroup, pageGroup: PageGroup) {
    this.router.navigate([
      'admin',
      'super-groups',
      pageSG.id,
      'page-groups',
      pageGroup.id,
      'pages'
    ]);
  }

  onPageGroupChange(pageSG: PageSuperGroup, pageGroup: PageGroup, changed: MatCheckboxChange) {
    // assign to user, add audit record, update user on screen, toast, drop from favorites if necessary
  }
}
