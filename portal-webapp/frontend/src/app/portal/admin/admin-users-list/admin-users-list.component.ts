import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatPaginator,
  MatSort,
  MatSlideToggleChange,
  MatSnackBar,
  MatSlideToggle
} from '@angular/material';
import { AdminUsersListDataSource } from './admin-users-list-datasource';
import { PortalUserService } from '../../../generated/service/portal-user.service';
import { CurrentUserService } from '../../../shared/services/current-user.service';
import { PortalUser } from '../../../shared/model/portal-user.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-users-list',
  templateUrl: './admin-users-list.component.html',
  styleUrls: ['./admin-users-list.component.css']
})
export class AdminUsersListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSlideToggle) isActive: MatSlideToggle;
  @ViewChild(MatSlideToggle) isAdmin: MatSlideToggle;
  dataSource: AdminUsersListDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['displayName', 'email', 'isAdmin'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: PortalUserService,
    private currentUserSerivce: CurrentUserService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.dataSource = new AdminUsersListDataSource(
      this.paginator,
      this.sort,
      this.userService,
      this.currentUserSerivce
    );
  }

  applyFilter(filterValue: string) {
    if (!this.dataSource.loadingComplete) {
      this.snackBar.open('Will filter once all the data has loaded');
    } else {
      this.dataSource.filter(
        filterValue,
        this.isAdmin.checked,
        this.isActive.checked
      );
    }
  }

  onNewUser() {}

  onInlucdeInActive(change: MatSlideToggleChange) {
    if (change.checked) {
      this.displayedColumns.push('isActive');
    } else {
      this.displayedColumns.pop();
    }
  }

  onUserSelect(user: PortalUser) {
    this.router.navigate([user.id], { relativeTo: this.route });
  }
}
