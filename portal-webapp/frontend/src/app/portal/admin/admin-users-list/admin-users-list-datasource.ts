import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map, switchMap, startWith } from 'rxjs/operators';
import {
  Observable,
  of as observableOf,
  merge,
  Subject,
  BehaviorSubject
} from 'rxjs';
import { PortalUser } from '../../../shared/model/portal-user.model';
import { PortalUserService } from '../../../generated/service/portal-user.service';
import { CurrentUserService } from '../../../shared/services/current-user.service';

// TODO: Replace this with your own data model type
export interface AdminUsersListItem {
  name: string;
  id: number;
}

/**
 * Data source for the AdminUsersList view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class AdminUsersListDataSource extends DataSource<PortalUser> {
  data: PortalUser[] = new Array<PortalUser>();
  public initialLoad = false;
  public loadingComplete = false;
  private emitter: BehaviorSubject<PortalUser[]>;

  constructor(
    private paginator: MatPaginator,
    private sort: MatSort,
    private userService: PortalUserService,
    private currentUserService: CurrentUserService
  ) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<PortalUser[]> {
    this.initialLoad = false;
    this.loadingComplete = false;
    const currentUser: PortalUser = this.currentUserService.getCurrentUser();
    const currentUserArray = [currentUser];
    this.emitter = new BehaviorSubject(currentUserArray);

    this.loadInitialData();

    return this.emitter;
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.

    // const dataMutations = [
    //   observableOf({}),
    //   this.paginator.page,
    //   this.sort.sortChange
    // ];

    // Set the paginators length
    // this.paginator.length = this.data.length;

    // return merge(this.sort.sortChange, this.paginator.page).pipe(
    //   startWith(this.currentUserService.getCurrentUser()),
    //   switchMap(() => {
    //     return this.userService.findAll();
    //   }),
    //   map((data: PortalUser[]) => {
    //     return this.getPagedData(this.getSortedData(data));
    //   })
    // );
  }

  loadInitialData() {
    // TODO: replace with logic for just getting the first page of users
    const currentUser: PortalUser = this.currentUserService.getCurrentUser();
    if (currentUser.id) {
      this.userService
        .getPortalUser(currentUser.id)
        .subscribe((user: PortalUser) => {
          this.initialLoad = true;
          this.data = [user];
          this.emitPagedAndSortedData(this.data);
          this.loadAllData();
        });
    } else {
      this.initialLoad = true;
      this.loadAllData();
    }
  }

  loadAllData() {
    // TODO: replace with logic for getting the full list of users if not using find all
    this.userService.findAll().subscribe((users: PortalUser[]) => {
      this.loadingComplete = true;
      this.data = users;
      this.emitPagedAndSortedData(this.data);
    });
  }

  filter(search: string, isAdmin: boolean, isActive: boolean) {
    const searchLower = search.toLowerCase();
    const data = this.data.filter((user: PortalUser) => {
      const displayNameMatch = user.displayName
        .toLowerCase()
        .includes(searchLower);
      const emailMatch = user.email.toLowerCase().includes(searchLower);
      // do not include admins if unchecked/false
      const isAdminMatch = isAdmin || !user.admin;
      // if unchecked, then the user must be active
      const isActiveMatch = isActive || user.active;
      return (displayNameMatch || emailMatch) && isAdminMatch && isActiveMatch;
    });
    this.emitPagedAndSortedData(data);
  }

  emitPagedAndSortedData(data: PortalUser[]) {
    const pagedData = this.getPagedData(this.getSortedData(data));
    this.emitter.next(pagedData);
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: PortalUser[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: PortalUser[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'displayName':
          return compare(a.displayName, b.displayName, isAsc);
        case 'email':
          return compare(a.email, b.email, isAsc);
        default:
          return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string, b: string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
