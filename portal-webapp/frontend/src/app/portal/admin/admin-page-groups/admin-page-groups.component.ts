import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PageSuperGroup } from '../../../shared/model/page-super-group.model';
import { PageGroup } from '../../../shared/model/page-group.model';
import { PageSuperGroupService } from '../../../generated/service/page-super-group.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin-page-groups',
  templateUrl: './admin-page-groups.component.html',
  styleUrls: ['./admin-page-groups.component.css']
})
export class AdminPageGroupsComponent implements OnInit {
  pageSuperGroup: PageSuperGroup;
  loading: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pageSGService: PageSuperGroupService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    // set the super group
    this.loading = true;
    this.pageSGService
      .getPageSuperGroup(this.route.snapshot.params['pageSuperGroupId'])
      .subscribe((pageSG: PageSuperGroup) => {
        this.pageSuperGroup = pageSG;
        this.loading = false;
      });
  }

  onEditPages(pageGroup: PageGroup) {
    this.router.navigate([pageGroup.id, 'pages'], {
      relativeTo: this.route
    });
  }

  onSavePageGroup(pageGroup: PageGroup) {
    this.pageSGService
      .savePageSuperGroup(this.pageSuperGroup)
      .subscribe((pageSGUpdated: PageSuperGroup) => {
        this.pageSuperGroup = pageSGUpdated;
        this.snackBar.open(pageGroup.name + ' was saved!');
      });
  }

  onNewPageGroup() {
    const newPageGroup = new PageGroup();
    newPageGroup.name = 'New Page Group';
    newPageGroup.description = 'Do not forget to save';
    this.pageSuperGroup.pageGroups.push(newPageGroup);
  }
}
