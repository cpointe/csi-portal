import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataInitService } from '../../shared/services/data-init.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute, private dataInitService: DataInitService) {}

  ngOnInit() {}

  loadSubPage(subPage: string) {
    this.router.navigate([subPage], { relativeTo: this.route });
  }

  onResetPages() {
    this.dataInitService.initializePages();
  }
  onDeletePages() {
    this.dataInitService.wipePages();
  }
}
