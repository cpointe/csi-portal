import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { OpenPagesService } from '../shared/services/open-pages.service';
import { PageSuperGroupService } from '../generated/service/page-super-group.service';
import { CurrentUserService } from '../shared/services/current-user.service';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.css']
})
export class PortalComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private breakpointObserver: BreakpointObserver,
    public router: Router,
    public route: ActivatedRoute,
    public openPagesService: OpenPagesService,
    public pagesService: PageSuperGroupService,
    public currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {}

  onHome() {
    this.router.navigate(['/home']);
  }
}
