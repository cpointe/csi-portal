import { Component, OnInit, Input } from '@angular/core';
import { Page } from '../../shared/model/page.model';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  @Input() page: Page;
  public pageUrl: SafeResourceUrl;

  constructor(public router: Router, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.pageUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.page.url);
  }

}
