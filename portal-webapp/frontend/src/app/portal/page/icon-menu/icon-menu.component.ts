import { Component, OnInit, Input } from '@angular/core';
import { Page } from '../../../shared/model/page.model';
import { OpenPagesService } from '../../../shared/services/open-pages.service';

@Component({
  selector: 'app-icon-menu',
  templateUrl: './icon-menu.component.html',
  styleUrls: ['./icon-menu.component.css']
})
export class IconMenuComponent implements OnInit {

  @Input() page: Page;

  constructor(public openPagesService: OpenPagesService) { }

  ngOnInit() {
  }

}
