import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../../../shared/services/current-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.css']
})
export class AccountMenuComponent implements OnInit {
  constructor(
    public currentUserService: CurrentUserService,
    private router: Router,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {}

  onShowAccount() {
    this.snackBar.open('TODO: Show Account Info');
  }

  onLogout() {
    this.snackBar.open('TODO: Logout');
  }

  onShare() {
    this.snackBar.open('TODO: Share');
  }

  onAdmin() {
    this.router.navigate(['admin']);
  }

  isAdmin() {
    return this.currentUserService.getCurrentUser().admin;
  }
}
