import { Component, OnInit } from '@angular/core';
import { Page } from '../../../shared/model/page.model';
import { OpenPagesService } from '../../../shared/services/open-pages.service';

@Component({
  selector: 'app-card-favorites',
  templateUrl: './card-favorites.component.html',
  styleUrls: ['./card-favorites.component.css']
})
export class CardFavoritesComponent implements OnInit {

  favoritePages: Page[];

  constructor(public openPageService: OpenPagesService) { }

  ngOnInit() {
    this.favoritePages = new Array<Page>();
    let page = new Page();
    page.name = 'Bing';
    page.description = 'Bing Search Engine';
    page.id = 'bing123';
    page.iconName = 'search';
    page.nameShort = 'Bing';
    page.url = 'http://www.bing.com';
    this.favoritePages.push(page);

    page = new Page();
    page.description = 'Counterpointe Solutions Inc. Home Page';
    page.iconName = 'domain';
    page.id = 'cpointe123';
    page.name = 'Counterpointe Solutions Inc.';
    page.nameShort = 'Cpointe';
    page.url = 'http://cpointe-inc.com/';
    this.favoritePages.push(page);
  }

  openPage(page: Page) {
    this.openPageService.openPage(page);
  }

}
