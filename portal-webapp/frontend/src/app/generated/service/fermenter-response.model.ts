import { FermenterMessage } from './fermenter-message.model';

export class FermenterResponse {
  public messages: FermenterMessage[] = new Array<FermenterMessage>();
  public value: Object;

  constructor(response: IFermenterResponseBody) {
    for (const message of response.messages.messages) {
      this.messages.push(new FermenterMessage(message));
    }
    this.value = response.value;
  }
}

export interface IFermenterResponseBody {
  messages: IFermenterMessages;
  value: Object;
}

export interface IFermenterMessages {
  messages: Array<Object>;
}
