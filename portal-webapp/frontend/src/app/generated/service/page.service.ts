import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { map, filter, scan, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IFermenterResponseBody, FermenterResponse } from './fermenter-response.model';
import { Constants } from '../../constants';

import { PageBase } from '../model/page-base.model';
import { Page } from '../../shared/model/page.model';

/*******************************************************
* Maintenance (CRUD) services for Page
*
* Generated Code - DO NOT MODIFY
*******************************************************/

@Injectable({
  providedIn: 'root'
})
export class PageService {
  private PageURL: string;

  constructor(private http: HttpClient, private constants: Constants) {
    this.PageURL = this.constants.portalDomainEndPoint + 'Page';
  }

  public getPage(id: string):
          Observable<{}|Array<Page>> {
    const url = this.PageURL + '/' + id;

    return this.http.get<IFermenterResponseBody>(url).pipe(
      map((response: IFermenterResponseBody) => this.parseResponseToSingle(response)),
      catchError(this.handleError)
    );
  }

  public findAll(): Observable<{}|Array<Page>> {
    const url = this.PageURL;

    return this.http.get<IFermenterResponseBody>(url).pipe(
      map((response: IFermenterResponseBody) => this.parseResponseToArray(response)),
      catchError(this.handleError)
    );
  }

  /**
   * Will create or update the entity based on whether or not it has an ID.
   *
   * @param page
   */
  public savePage(page: Page):
          Observable<{}|Page> {
    let serviceCall;
    if (page.id) {
      serviceCall = this.updatePage(page);
    } else {
      serviceCall = this.createPage(page);
    }
    return serviceCall;
  }


  /**
   * Will create the entity using the maintenance services.
   *
   * @param page
   */
  public createPage(page: Page):
          Observable<{}|Page> {
    const url = this.PageURL;

    return this.http.post(url, page).pipe(
      map((response: IFermenterResponseBody) => this.parseResponseToSingle(response)),
      catchError(this.handleError)
    );
  }

  /**
   * Will update the entity using the maintenance services.
   *
   * @param page
   */
  public updatePage(page: Page):
          Observable<{}|Page> {
    const url = this.PageURL + '/' + page.id;

    return this.http.put(url, page).pipe(
      map((response: IFermenterResponseBody) => this.parseResponseToSingle(response)),
      catchError(this.handleError)
    );
  }

  /**
   * Will delete the entity using the maintenance services.
   *
   * @param page
   */
  public deletePage(page: Page):
          Observable<{}|Page> {
    const url = this.PageURL + '/' + page.id;

    return this.http.delete(url).pipe(
      map((response: IFermenterResponseBody) => this.parseResponseToSingle(response)),
      catchError(this.handleError)
    );
  }

  private parseResponseToArray(response: IFermenterResponseBody): Array<Page> {
    const objects: Page[] = new Array<Page>();
    const fermenterResponse = new FermenterResponse(response);
    if (!fermenterResponse) {
      console.log('Looks like there may have been an issue.');
    } else if (fermenterResponse.messages.length > 0) {
      // TODO: alert the user that there were issues
    } else {
      // if response.value is an array or work Orders parse each
      const baseObjects = <Array<Page>>response.value;
      for (const baseObject of baseObjects) {
        objects.push(new Page(baseObject));
      }
    }
    return objects;
  }

  private parseResponseToSingle(response: IFermenterResponseBody): Page {
    let object: Page;
    const fermenterResponse = new FermenterResponse(response);
    if (!fermenterResponse) {
      console.log('Looks like there may have been an issue.');
    } else if (fermenterResponse.messages.length > 0) {
      // TODO: alert the user that there were issues
    } else {
      object = new Page(<PageBase> fermenterResponse.value);
    }
    return object;
  }

  // TODO: move to an error service
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return Observable.throw(
      'Something bad happened; please try again later.');
  }
}
