import { Favorite } from '../../shared/model/favorite.model';
import { AccessHistory } from '../../shared/model/access-history.model';

/*******************************************************
* Base BO for Angular frontend for Page
*
* Generated Code - DO NOT MODIFY
*******************************************************/

export class PageBase {

  // Fields
  name: string;
  description: string;
  id: string;
  iconName: string;
  nameShort: string;
  url: string;

  // Associations
  favorites: Favorite[] = new Array<Favorite>();
  accessHistorys: AccessHistory[] = new Array<AccessHistory>();

  constructor(pageBase?: PageBase) {
    if (pageBase) {
      this.name = pageBase.name;
      this.description = pageBase.description;
      this.id = pageBase.id;
      this.iconName = pageBase.iconName;
      this.nameShort = pageBase.nameShort;
      this.url = pageBase.url;

      if (pageBase.favorites) {
        for (const favorite of pageBase.favorites) {
          this.favorites.push(new Favorite(favorite));
        }
      }

      if (pageBase.accessHistorys) {
        for (const accessHistory of pageBase.accessHistorys) {
          this.accessHistorys.push(new AccessHistory(accessHistory));
        }
      }
    }
  }
}
