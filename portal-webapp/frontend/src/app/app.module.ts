import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatFormFieldModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS
} from '@angular/material';
import { PortalComponent } from './portal/portal.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HomeComponent } from './portal/home/home.component';
import { PageComponent } from './portal/page/page.component';
import { CardFavoritesComponent } from './portal/cards/card-favorites/card-favorites.component';
import { EmptyComponent } from './shared/empty/empty.component';
import { IconMenuComponent } from './portal/page/icon-menu/icon-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { AccountMenuComponent } from './portal/account/account-menu/account-menu.component';
import { AccountInfoComponent } from './portal/account/account-info/account-info.component';
import { CurrentUserService } from './shared/services/current-user.service';
import { AdminComponent } from './portal/admin/admin.component';
import { AdminUserComponent } from './portal/admin/admin-user/admin-user.component';
import { FormsModule } from '@angular/forms';
import { AdminSuperGroupsComponent } from './portal/admin/admin-super-groups/admin-super-groups.component';
import { AdminPageGroupsComponent } from './portal/admin/admin-page-groups/admin-page-groups.component';
import { AdminPagesComponent } from './portal/admin/admin-pages/admin-pages.component';
import { AdminUsersListComponent } from './portal/admin/admin-users-list/admin-users-list.component';

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    HomeComponent,
    PageComponent,
    CardFavoritesComponent,
    EmptyComponent,
    IconMenuComponent,
    AccountMenuComponent,
    AccountInfoComponent,
    AdminComponent,
    AdminSuperGroupsComponent,
    AdminUserComponent,
    AdminPageGroupsComponent,
    AdminPagesComponent,
    AdminUsersListComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    LayoutModule,
    // Material components used
    MatFormFieldModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
  ],
  providers: [CurrentUserService, {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500}}],
  bootstrap: [AppComponent]
})
export class AppModule {}
