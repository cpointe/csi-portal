import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from './shared/services/current-user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // initialize the user as the first part of the app initialization
  constructor(private currentUserService: CurrentUserService) {
    console.log('logged in as: ' + this.currentUserService.getCurrentUser());
  }

  ngOnInit() {}
}
