import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './portal/home/home.component';
import { EmptyComponent } from './shared/empty/empty.component';
import { AdminComponent } from './portal/admin/admin.component';
import { AdminSuperGroupsComponent } from './portal/admin/admin-super-groups/admin-super-groups.component';
import { AdminPageGroupsComponent } from './portal/admin/admin-page-groups/admin-page-groups.component';
import { AdminPagesComponent } from './portal/admin/admin-pages/admin-pages.component';
import { AdminUsersListComponent } from './portal/admin/admin-users-list/admin-users-list.component';
import { AdminUserComponent } from './portal/admin/admin-user/admin-user.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'admin', component: AdminComponent },
  {
    path: 'admin/users',
    children: [
      { path: ':userId', component: AdminUserComponent },
      { path: '', component: AdminUsersListComponent, pathMatch: 'full' }
    ]
  },
  {
    path: 'admin/super-groups',
    children: [
      {
        path: ':pageSuperGroupId/page-groups',
        children: [
          { path: ':pageGroupId/pages', component: AdminPagesComponent },
          { path: '', component: AdminPageGroupsComponent, pathMatch: 'full' }
        ]
      },
      { path: '', component: AdminSuperGroupsComponent, pathMatch: 'full' }
    ]
  },
  // empty component is used as we don't use router-outlet so that we don't need to reload sub-pages
  { path: 'page/:pageId', component: EmptyComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
