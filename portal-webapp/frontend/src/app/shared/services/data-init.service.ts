import { Injectable } from '@angular/core';
import { PageSuperGroupService } from '../../generated/service/page-super-group.service';
import { PageSuperGroup } from '../model/page-super-group.model';
import { PortalUserService } from '../../generated/service/portal-user.service';
import { PortalUser } from '../model/portal-user.model';
import { DemoPageData } from './demo-data-pages';
import { PageSuperGroupBase } from '../../generated/model/page-super-group-base.model';

@Injectable({
  providedIn: 'root'
})
export class DataInitService {
  constructor(
    private pageSGService: PageSuperGroupService,
    private userService: PortalUserService
  ) {}

  public initializePages() {
    this.wipePages();
    const demoPageData = new DemoPageData;
    for (const pageSG of <Array<PageSuperGroupBase>>demoPageData.data) {
      const pageSGTemp = new PageSuperGroup(pageSG);
      this.pageSGService.createPageSuperGroup(pageSGTemp).subscribe();
    }
  }

  public wipePages() {
    this.pageSGService.findAll().subscribe((pageSGs: PageSuperGroup[]) => {
      pageSGs.forEach(pageSg =>
        this.pageSGService.deletePageSuperGroup(pageSg).subscribe()
      );
    });
    this.userService.findAll().subscribe((users: PortalUser[]) => {
      users.forEach(user =>
        this.userService.deletePortalUser(user).subscribe()
      );
    });
  }
}
