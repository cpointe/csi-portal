import { Injectable, OnInit } from '@angular/core';
import { Page } from '../model/page.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PageService } from '../../generated/service/page.service';

@Injectable({
  providedIn: 'root'
})
export class OpenPagesService implements OnInit {
  public openPages = new Array<Page>();

  constructor(private router: Router, private route: ActivatedRoute, private pageService: PageService) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      if (params.has('pageId') && this.router.url.includes('/page')) {
        const pageId = params.get('pageId');
        this.pageService.getPage(pageId).subscribe((page: Page) => {
          this.openPage(page);
        });
      }
    });
  }

  public openPage(page: Page) {
    if (!this.openPages.includes(page)) {
      this.openPages.push(page);
    }
    this.router.navigate(['page', page.id]);
  }

  public closePage(page: Page) {
    const index = this.openPages.indexOf(page);
    this.openPages.splice(index, 1);
    if (this.route.snapshot.children[0].params['pageId'] === page.id) {
      this.router.navigate(['home']);
    }
  }

  public pageIsOpen(page: Page): Boolean {
    if (this.openPages.includes(page)) {
      return true;
    } else {
      return false;
    }
  }

  public multiplePagesOpen(): Boolean {
    if (this.openPages.length > 1) {
      return true;
    } else {
      return false;
    }
  }

  public closeAll() {
    this.openPages = new Array<Page>();
    this.router.navigate(['home']);
  }

  public closeOthers(page: Page) {
    const tempArray = new Array<Page>();
    tempArray.push(page);
    this.openPages = tempArray;
  }
}
