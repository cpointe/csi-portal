import { Injectable, OnInit } from '@angular/core';
import { PortalUserService } from '../../generated/service/portal-user.service';
import { PortalUser } from '../model/portal-user.model';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService implements OnInit {
  private currentUser: PortalUser;
  constructor(private portalUserService: PortalUserService) {}

  ngOnInit() {
    this.initializeUser();
  }

  private initializeUser() {
    if (!this.currentUser || this.currentUser.displayName === '') {
      // TODO: get the current user from the backend.
      const newUser = new PortalUser();
      newUser.displayName = 'Edward Robinson';
      newUser.active = true;
      newUser.email = 'testing@test-me.com';
      newUser.admin = true;
      this.currentUser = newUser;
      this.portalUserService
        .createPortalUser(newUser)
        .subscribe((user: PortalUser) => {
          this.currentUser = user;
        });
    }
  }

  public getCurrentUser() {
    this.initializeUser();
    return this.currentUser;
  }
}
