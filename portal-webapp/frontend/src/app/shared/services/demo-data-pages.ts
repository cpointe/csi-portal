export class DemoPageData {
  public data = [
    {
      name: 'External',
      description: 'Pages from out on the internet',
      pageGroups: [
        {
          name: 'Search Engines',
          description: 'This group includes common pages',
          pages: [
            {
              name: 'Bing Search Engine',
              description: 'Microsoft Search Engine',
              iconName: 'search',
              nameShort: 'Bing',
              url: 'http://bing.com'
            },
            {
              name: 'Duck Duck Go',
              description: 'Who knew?',
              iconName: 'search',
              nameShort: 'Duck',
              url: 'https://duckduckgo.com'
            },
            {
              name: 'Google - Does not work',
              description:
                '#1 Search Engine - Google does not support loading within a frame',
              iconName: 'error_outline',
              nameShort: 'Google',
              url: 'https://google.com'
            }
          ]
        },
        {
          name: 'Social Media',
          description: 'Popular social media sites',
          pages: [
            {
              name: 'Facebook',
              description:
                'Facebook is a social networking site that makes it easy for you to connect and share with your family and friends online.',
              iconName: 'group',
              nameShort: 'Bing',
              url: 'http://bing.com'
            },
            {
              name: 'Youtube',
              description: 'Popular video sharing site.',
              iconName: 'video_library',
              nameShort: 'Youtube',
              url: 'https://duckduckgo.com'
            },
            {
              name: 'Twitter',
              description: 'Yeah, about that.',
              iconName: 'mood',
              nameShort: 'Twitter',
              url: 'https://google.com'
            }
          ]
        }
      ]
    },
    {
      name: 'Programming Pages',
      description: 'Pages that are used for programming',
      pageGroups: [
        {
          name: 'Source Control',
          description:
            'Tools that are commonly used for source control management.',
          pages: [
            {
              name: 'Git Home Page',
              description: 'Git home page',
              iconName: 'file_copy',
              nameShort: 'Git',
              url: 'https://git-scm.com/'
            },
            {
              name: 'Bitbucket',
              description: 'Bitbucket home page',
              iconName: 'folder_shared',
              nameShort: 'Bitbucket',
              url: 'https://bitbucket.org'
            },
            {
              name: 'Github',
              description: 'Github home page',
              iconName: 'folder_shared',
              nameShort: 'Github',
              url: 'https://git-scm.com/'
            }
          ]
        },
        {
          name: 'Help',
          description:
            'Turorials and help pages that are commonly used for programming.',
          pages: [
            {
              name: 'Stack Overflow',
              description: 'Page that is popular for question and answers.',
              iconName: 'help',
              nameShort: 'StackOverflow',
              url: 'https://stackoverflow.com'
            },
            {
              name: 'Udemy',
              description: 'Udemy home page',
              iconName: 'help_outline',
              nameShort: 'Udemy',
              url: 'https://www.udemy.com/'
            }
          ]
        }
      ]
    }
  ];
}
