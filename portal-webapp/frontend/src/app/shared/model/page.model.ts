import { PageBase } from '../../generated/model/page-base.model';

/*******************************************************
 * BO for Angular frontend for Page
 *
 * Generated Code - DO MODIFY
 *******************************************************/

export class Page extends PageBase {
  constructor(page?: PageBase) {
    super(page);
  }

  public isFavorite(): Boolean {
    if (this.id === 'bing123') {
      return true;
    } else {
      return false;
    }
  }
}
