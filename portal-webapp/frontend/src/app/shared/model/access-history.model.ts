import { AccessHistoryBase } from '../../generated/model/access-history-base.model';

/*******************************************************
* BO for Angular frontend for AccessHistory
*
* Generated Code - DO MODIFY
*******************************************************/

export class AccessHistory extends AccessHistoryBase {
  constructor(accessHistory?: AccessHistoryBase) {
    super(accessHistory);
  }
}
