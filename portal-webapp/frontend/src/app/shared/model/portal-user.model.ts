import { PortalUserBase } from '../../generated/model/portal-user-base.model';

/*******************************************************
* BO for Angular frontend for PortalUser
*
* Generated Code - DO MODIFY
*******************************************************/

export class PortalUser extends PortalUserBase {
  constructor(portalUser?: PortalUserBase) {
    super(portalUser);
  }
}
