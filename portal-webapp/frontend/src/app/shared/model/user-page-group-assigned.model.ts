import { UserPageGroupAssignedBase } from '../../generated/model/user-page-group-assigned-base.model';

/*******************************************************
* BO for Angular frontend for UserPageGroupAssigned
*
* Generated Code - DO MODIFY
*******************************************************/

export class UserPageGroupAssigned extends UserPageGroupAssignedBase {
  constructor(userPageGroupAssigned?: UserPageGroupAssignedBase) {
    super(userPageGroupAssigned);
  }
}
