import { PageSuperGroupBase } from '../../generated/model/page-super-group-base.model';

/*******************************************************
 * BO for Angular frontend for PageSuperGroup
 *
 * Generated Code - DO MODIFY
 *******************************************************/

export class PageSuperGroup extends PageSuperGroupBase {
  constructor(pageSuperGroup?: PageSuperGroupBase) {
    super(pageSuperGroup);
  }
  public getPageGroup(pageGroupId: string) {
    if (!this.pageGroups || this.pageGroups.length === 0) {
      return null;
    } else {
      return this.pageGroups.find(p => p.id === pageGroupId);
    }
  }
}
