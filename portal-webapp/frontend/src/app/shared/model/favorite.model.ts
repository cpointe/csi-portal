import { FavoriteBase } from '../../generated/model/favorite-base.model';

/*******************************************************
* BO for Angular frontend for Favorite
*
* Generated Code - DO MODIFY
*******************************************************/

export class Favorite extends FavoriteBase {
  constructor(favorite?: FavoriteBase) {
    super(favorite);
  }
}
