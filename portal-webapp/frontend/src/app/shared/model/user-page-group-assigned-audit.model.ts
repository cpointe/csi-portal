import { UserPageGroupAssignedAuditBase } from '../../generated/model/user-page-group-assigned-audit-base.model';

/*******************************************************
* BO for Angular frontend for UserPageGroupAssignedAudit
*
* Generated Code - DO MODIFY
*******************************************************/

export class UserPageGroupAssignedAudit extends UserPageGroupAssignedAuditBase {
  constructor(userPageGroupAssignedAudit?: UserPageGroupAssignedAuditBase) {
    super(userPageGroupAssignedAudit);
  }
}
