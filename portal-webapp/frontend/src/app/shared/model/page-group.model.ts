import { PageGroupBase } from '../../generated/model/page-group-base.model';

/*******************************************************
* BO for Angular frontend for PageGroup
*
* Generated Code - DO MODIFY
*******************************************************/

export class PageGroup extends PageGroupBase {
  constructor(pageGroup?: PageGroupBase) {
    super(pageGroup);
  }
}
