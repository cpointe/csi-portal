package com.csi.portal.domain.service.impl;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.csi.portal.domain.bizobj.AccessHistoryBO;
import com.csi.portal.domain.bizobj.PageBO;
import com.csi.portal.domain.bizobj.PageGroupBO;
import com.csi.portal.domain.service.rest.UserService;

/**
 * Service implementation class for the User service.
 * 
 * @see com.csi.portal.domain.service.rest.UserService
 *
 *      GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Service
public class UserServiceImpl extends UserBaseServiceImpl implements UserService {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<PageGroupBO> getAvailablePageGroupsForUserImpl() {
        // may not be necessary with userBO setup
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Boolean openPageImpl(PageBO page) {
        AccessHistoryBO history = new AccessHistoryBO(page);
        history.save();
        return true;
    }

}