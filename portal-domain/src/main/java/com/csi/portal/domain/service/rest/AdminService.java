package com.csi.portal.domain.service.rest;

import javax.ws.rs.Path;

/**
 * Interface for the Admin service that may be modified by 
 * developers to encapsulate any service operations that are not supported
 * for definition in this domain's meta-model.
 *
 * @see com.csi.portal.domain.service.rest.AdminBaseService
 * 
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Path("AdminService")
public interface AdminService extends AdminBaseService {
	
	// Developers should add any service operations here that cannot be defined via the PIM
	
}