package com.csi.portal.domain.bizobj;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.bitbucket.fermenter.stout.util.SpringAutowiringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Business object for the Favorite entity.
 * @see com.csi.portal.domain.bizobj.FavoriteBaseBO
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Entity
@Table(name="FAVORITE")
public class FavoriteBO extends FavoriteBaseBO {
	
	public FavoriteBO() {
		super();
		SpringAutowiringUtil.autowireBizObj(this);
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FavoriteBO.class);
	
	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected void complexValidation() {

	}

    public boolean isInPageGroup(PageGroupBO pageGroup) {
        if(getPage().getPageGroup().getKey() == pageGroup.getKey()) {
            return true;
        } else {
            return false;
        }
    }
	
}