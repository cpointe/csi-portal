package com.csi.portal.domain.bizobj;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.bitbucket.fermenter.stout.util.SpringAutowiringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Business object for the AccessHistory entity.
 * @see com.csi.portal.domain.bizobj.AccessHistoryBaseBO
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Entity
@Table(name="ACCESS_HISTORY")
public class AccessHistoryBO extends AccessHistoryBaseBO {
	
	public AccessHistoryBO() {
		super();
		SpringAutowiringUtil.autowireBizObj(this);
	}
	
	public AccessHistoryBO(PageBO page) {
        setPage(page);
        setPortalUser(PortalUserBO.getCurrentUser());
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessHistoryBO.class);
	
	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected void complexValidation() {

	}
	
}