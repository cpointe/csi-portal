package com.csi.portal.domain.service.rest;

import javax.ws.rs.Path;

/**
 * Interface for the User service that may be modified by 
 * developers to encapsulate any service operations that are not supported
 * for definition in this domain's meta-model.
 *
 * @see com.csi.portal.domain.service.rest.UserBaseService
 * 
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Path("UserService")
public interface UserService extends UserBaseService {
	
	// Developers should add any service operations here that cannot be defined via the PIM
	
}