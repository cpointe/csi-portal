package com.csi.portal.domain.persist;

import java.util.UUID;

import com.csi.portal.domain.bizobj.FavoriteBO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Data access repository for the Favorite business object.
 * 
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */ 
public interface FavoriteRepository extends JpaRepository<FavoriteBO, UUID>, JpaSpecificationExecutor<FavoriteBO> {
	
	/**
	 * Developers should leverage this interface to define any query logic
	 * that cannot be realized through {@link JpaRepository}'s built-in
	 * functionality.  
	 */

}