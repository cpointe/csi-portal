package com.csi.portal.domain.bizobj;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.bitbucket.fermenter.stout.util.SpringAutowiringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * Business object for the PortalUser entity.
 * 
 * @see com.csi.portal.domain.bizobj.PortalUserBaseBO
 *
 *      GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Entity
@Table(name = "PORTAL_USER")
public class PortalUserBO extends PortalUserBaseBO {

    public PortalUserBO() {
        super();
        SpringAutowiringUtil.autowireBizObj(this);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalUserBO.class);

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected void complexValidation() {

    }

    /**
     * Assigns (grant == true) or unassigns (grant == false) the given user to the
     * given page group. This will additionally audit the change.
     * 
     * @param userParam
     * @param pageGroup
     * @param grant
     * @return
     */
    public static PortalUserBO updateUserPageGroupAccess(PortalUserBO userParam, PageGroupBO pageGroup, Boolean grant) {
        PortalUserBO user = PortalUserBO.findByPrimaryKey(userParam.getKey());

        UserPageGroupAssignedBO alreadyAssigned = user.getUserPageGroupAssigneds().stream()
                .filter(groupAssigned -> groupAssigned.getId() == pageGroup.getId()).findFirst().orElse(null);

        // TODO: move into the save/delete of a UserPageGroupAssignedBO
        UserPageGroupAssignedAuditBO audit = new UserPageGroupAssignedAuditBO(user, pageGroup, grant);

        if (alreadyAssigned == null && grant) {
            // if not already assigned, and trying to assign, then assign the
            // group
            UserPageGroupAssignedBO pageGroupAssigned = new UserPageGroupAssignedBO();
            pageGroupAssigned.setPageGroup(pageGroup);
            pageGroupAssigned.setPortalUser(user);
            pageGroupAssigned.save();
            
            // only save the audit if an action was taken
            audit.save();
        } else if (alreadyAssigned != null && !grant) {
            // if assigned, and trying to remove then remove
            removeNecessaryFavorites(user, pageGroup);
            alreadyAssigned.delete();
            
            // only save the audit if an action was taken
            audit.save();
        }

        // return the updated user
        return PortalUserBO.findByPrimaryKey(userParam.getKey());
    }

    private static void removeNecessaryFavorites(PortalUserBO user, PageGroupBO pageGroup) {
        Set<FavoriteBO> favorites = user.getFavorites();
        
        for(FavoriteBO favorite: favorites) {
            if(favorite.isInPageGroup(pageGroup)) {
                favorite.delete();
            }
        }
    }

    public static PortalUserBO getCurrentUser() {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = user.getUsername(); //get logged in username
        return PortalUserBO.findByEmail(email);
    }

    private static PortalUserBO findByEmail(String email) {
        return getDefaultRepository().findByEmail(email);
    }

}