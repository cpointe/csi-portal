package com.csi.portal.domain.bizobj;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.bitbucket.fermenter.stout.util.SpringAutowiringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Business object for the UserPageGroupAssigned entity.
 * @see com.csi.portal.domain.bizobj.UserPageGroupAssignedBaseBO
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Entity
@Table(name="USER_PAGE_GROUP_ASSIGNED")
public class UserPageGroupAssignedBO extends UserPageGroupAssignedBaseBO {
	
	public UserPageGroupAssignedBO() {
		super();
		SpringAutowiringUtil.autowireBizObj(this);
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserPageGroupAssignedBO.class);
	
	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected void complexValidation() {

	}
	
}