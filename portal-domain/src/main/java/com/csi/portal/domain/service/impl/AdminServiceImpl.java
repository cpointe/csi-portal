package com.csi.portal.domain.service.impl;

import org.springframework.stereotype.Service;

import com.csi.portal.domain.bizobj.*;
import com.csi.portal.domain.service.rest.AdminService;

/**
 * Service implementation class for the Admin service.
 * 
 * @see com.csi.portal.domain.service.rest.AdminService
 *
 *      GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Service
public class AdminServiceImpl extends AdminBaseServiceImpl implements AdminService {

    /**
     * assign to user, add audit record, drop from favorites if necessary.
     */
    @Override
    protected PortalUserBO updateUserPageGroupAccessImpl(PortalUserBO userParam, PageGroupBO pageGroup, Boolean grant) {
        return PortalUserBO.updateUserPageGroupAccess(userParam, pageGroup, grant);
    }

}