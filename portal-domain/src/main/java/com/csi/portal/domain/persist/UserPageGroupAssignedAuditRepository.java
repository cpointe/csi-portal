package com.csi.portal.domain.persist;

import java.util.UUID;

import com.csi.portal.domain.bizobj.UserPageGroupAssignedAuditBO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Data access repository for the UserPageGroupAssignedAudit business object.
 * 
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */ 
public interface UserPageGroupAssignedAuditRepository extends JpaRepository<UserPageGroupAssignedAuditBO, UUID>, JpaSpecificationExecutor<UserPageGroupAssignedAuditBO> {
	
	/**
	 * Developers should leverage this interface to define any query logic
	 * that cannot be realized through {@link JpaRepository}'s built-in
	 * functionality.  
	 */

}