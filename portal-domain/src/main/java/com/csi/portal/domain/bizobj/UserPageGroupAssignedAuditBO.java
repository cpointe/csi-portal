package com.csi.portal.domain.bizobj;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.bitbucket.fermenter.stout.util.SpringAutowiringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Business object for the UserPageGroupAssignedAudit entity.
 * @see com.csi.portal.domain.bizobj.UserPageGroupAssignedAuditBaseBO
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
@Entity
@Table(name="USER_PAGE_GROUP_ASSIGNED_AUDIT")
public class UserPageGroupAssignedAuditBO extends UserPageGroupAssignedAuditBaseBO {
	
	public UserPageGroupAssignedAuditBO() {
		super();
		SpringAutowiringUtil.autowireBizObj(this);
	}
	
	public UserPageGroupAssignedAuditBO(PortalUserBO user, PageGroupBO pageGroup, Boolean grant) {
	    setPageGroup(pageGroup);
        setPortalUser(user);
        setPageGroupAdded(grant);
        setUpdateTime(new Timestamp(System.currentTimeMillis()));
        setUserTakingAction(PortalUserBO.getCurrentUser());
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(UserPageGroupAssignedAuditBO.class);
	
	@Override
	protected Logger getLogger() {
		return LOGGER;
	}

	@Override
	protected void complexValidation() {

	}
	
}